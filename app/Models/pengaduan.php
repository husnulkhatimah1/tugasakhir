<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

    class pengaduan extends Model
    {
        protected $table = "pengaduans";
        protected $primarykey = "id";
        protected $fillable = [
            'id','email_id', 'nama_pelapor', 'unit_kerja_pelapor', 'jabatan_pelapor','lokasi1', 'tanggal_kejadian', 'uraian_lengkap_kejadian', 'dugaan_jenis_pelanggaran', 'waktu_perkiraan_kejadian','status'
        ];

        public function email()
        {
            return $this->belongsTo(User::class);
        }

        public function getKoordinat()
        {
            $koordinat = explode(',', $this->lokasi);

            return [
                'lat' => $koordinat[0],
                'lng' => $koordinat[1],
            ];
        }
    }
