<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jenispelanggaran extends Model
{
    protected $table = "jenispelanggarans";
    protected $primarykey = "id";
    protected $fillable = [
        'id', 'jenispelanggaran'
    ];
     
    public function tambahdt(){
        return $this->hasMany(tambahdt::class,'jenis_pelanggaran','id');
    }
}
