<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tambahdt extends Model
{
    protected $table = "tambahdts";
    protected $primarykey = "id";
    protected $fillable = [
        'tanggal_kejadian','foto_kejadian','jenis_pelanggaran', 'nama_pelanggaran', 'lokasi_pelanggaran','berita'
    ];

    public function jenispelanggaran(){
        return $this->belongsTo('App\Models\jenispelanggaran');
    }
}
