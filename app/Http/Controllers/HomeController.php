<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\tambahdt;
use App\Models\jenispelanggaran;
use App\Models\pengaduan;
use App\Models\User;
use App\Mail\PengaduanUpdated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use PDF;


use Session;
  
class HomeController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->User();

        if($user->role('Administrator')) {
            return redirect('dashadmin');
        }
        elseif($user->role('User')) {
            $jenis=jenispelanggaran::all();
            return view('pengaduan',compact('jenis'));
        }
    }

    public function tambah1(Request $request)
    {
        $request->validate([
            'foto_kejadian' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff|max:1096' // 1096 = 1mb,
        ]);
        
        $file = $request->file('foto_kejadian');
        if ($file !== null) {
            $imageName = time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('image'), $imageName);
        } else {
            $imageName = 'tidak ada gambar';
        }
        
        // $nama_file = $_FILES["foto_kejadian"]["name"];
        // $ukuran_file = $_FILES["foto_kejadian"]["size"];
        // $tmp_file = $_FILES["foto_kejadian"]["tmp_name"];
        // $error = $_FILES["foto_kejadian"]["error"];

        // $ekstensi_diperbolehkan = array("jpg", "jpeg", "png", "gif");
        // $ekstensi_file = strtolower(pathinfo($nama_file, PATHINFO_EXTENSION));
        // if(!in_array($ekstensi_file, $ekstensi_diperbolehkan)) {
        //     echo "Error: Hanya file gambar yang diperbolehkan.";
        //     exit;
        // }

        // // Periksa ukuran file
        // $ukuran_diperbolehkan = 1024 * 1024; // 1 MB
        // if($ukuran_file > $ukuran_diperbolehkan) {
        //     echo "Error: Ukuran file terlalu besar. Maksimum 1 MB.";
        //     exit;
        // }
        // function uploaded_file($tmp_file, $destination) {
        //     if (move_uploaded_file($tmp_file, $destination)) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        
        // $direktori_upload = "public/";
        // $nama_file_baru = uniqid() . '.' . $ekstensi_file;
        // $file_upload = uploaded_file($tmp_file,public_path('image') . $nama_file_baru);

        $validateData['excerpt'] = strip_tags($request->berita);


        tambahdt::create([
            'tanggal_kejadian' => $request->tanggal_kejadian,
            'foto_kejadian' => $imageName,
            'jenis_pelanggaran' => $request->jenis_pelanggaran,
            'nama_pelanggaran' => $request->nama_pelanggaran,
            'lokasi_pelanggaran' => $request->lokasi_pelanggaran,
            'sangsi_pelanggaran' => $request->sangsi_pelanggaran,
            'berita' => $validateData['excerpt'],
        ]);
        Session::flash('message', 'berhasil di simpan');
        return redirect('tambahdt');
    }

    // public function tbpengaduan(Request $request)
    // {
        
    //     pengaduan::create([
    //         'nama_pelapor' => $request->nama_pelapor,
    //         'unit_kerja_pelapor' => $request->unit_kerja_pelapor,
    //         'jabatan_pelapor' => $request->jabatan_pelapor,
    //         'lokasi' => $request->lokasi,
    //         'tanggal_kejadian' => $request->tanggal_kejadian,
    //         'uraian_lengkap_kejadian' => $request->uraian_lengkap_kejadian,
    //         'dugaan_jenis_pelanggaran' => $request->dugaan_jenis_pelanggaran,
    //         'waktu_perkiraan_kejadian' => $request->waktu_perkiraan_kejadian,
           
    //     ]);
    //     Session::flash('message', 'berhasil di simpan');
    //     return redirect('pengaduan');
    // }

    public function tbpengaduan(Request $request)
    {
        // validasi form pengaduan
        $validatedData = $request->validate([
            'nama_pelapor' => 'required',
            'unit_kerja_pelapor' => 'required',
            'jabatan_pelapor' => 'required',
            'tanggal_kejadian' => 'required|date',
            'uraian_lengkap_kejadian' => 'required',
            'dugaan_jenis_pelanggaran' => 'required',
            'waktu_perkiraan_kejadian' => 'required',
            'lokasi1' => 'required',
        ]);

        // simpan data pengaduan ke database
        $pengaduan = new Pengaduan;
        $pengaduan->nama_pelapor = $request->nama_pelapor;
        $pengaduan->unit_kerja_pelapor = $request->unit_kerja_pelapor;
        $pengaduan->jabatan_pelapor = $request->jabatan_pelapor;
        $pengaduan->tanggal_kejadian = $request->tanggal_kejadian;
        $pengaduan->uraian_lengkap_kejadian = $request->uraian_lengkap_kejadian;
        $pengaduan->dugaan_jenis_pelanggaran = $request->dugaan_jenis_pelanggaran;
        $pengaduan->waktu_perkiraan_kejadian = $request->waktu_perkiraan_kejadian;
        $pengaduan->lokasi1 = $request->lokasi1;
        $pengaduan->email_id = $request->email;
        $pengaduan->status = $request->status;

        // tambahkan email pengguna yang sedang login
        // if (Auth::check()) {
        //     $pengaduan->email_id = Auth::user()->email;
        // }

        $pengaduan->save();

        Session::flash('message', 'berhasil di simpan');
        return redirect('pengaduan');
    }


    // public function jenispl(jenispelanggaran $jenis)
    // {
    //  $pelanggaran = $jenis->tambahdt()->get();   
    // $pelanggaran = tambahdt::where('jenis_pelanggaran', $jenis->id)->get();
    //     return view('Admin.detail_jenis_pelanggaran', compact('jenis', 'pelanggaran'));
    // }

    public function jenispl($id)
    {
        $jenis = jenispelanggaran::find($id);   
        $pelanggaran = tambahdt::where('jenis_pelanggaran', $jenis->id)->get();
        // dd($pelanggaran);
        return view('Admin.jenispl', compact('jenis', 'pelanggaran'));
    }


    public function cetakdata()
    {
        $jenis = jenispelanggaran::all();   
        $pelanggaran = tambahdt::all();
        // dd($pelanggaran);
        return view('Admin.cetakdata', compact('jenis', 'pelanggaran'));
    }
    public function jenispl1($id)
    {
        $jenis = jenispelanggaran::find($id);   
        $pelanggaran = tambahdt::where('id', $jenis->id)->get();
        // dd($pelanggaran);
        return view('Admin.jenispl', compact('jenis', 'pelanggaran'));
    }

    public function jenispl2($id)
    {
        $jenis = jenispelanggaran::find($id);   
        $pelanggaran = tambahdt::where('jenis_pelanggaran', $jenis->id)->get();
        // dd($pelanggaran);
        return view('jenispl2', compact('jenis', 'pelanggaran'));
    }

    public function editjenispl($id)
    {

        $jenis = jenispelanggaran::all();
        $pelanggaran = tambahdt::find($id);

        // $pelanggaran = tambahdt::where('jenispelanggaran', $pelanggaran)->first();
        // $jenis = jenispelanggaran::find($pelanggaran->id);


        // dd($pelanggaran);

        return view('Admin.editjenispl', compact('jenis', 'pelanggaran'));

    }

    public function simpaneditpl(Request $request, $id)
    {
        $jenis = jenispelanggaran::find($id);
        $pelanggaran = tambahdt::find($id);
        $validateData['excerpt'] = strip_tags($request->berita);
        // $request->validate([
        //     'foto_kejadian' => 'required|mimes:jpg,jpeg,png,bmp,tiff | max:1096' // 1096 = 1mb,
        // ]);
        // $file = $request->file('foto_kejadian');
        // $imageName = time().'.'.$file->getClientOriginalExtension();
        // $file->move(public_path('image'), $imageName);

        // Validasi file hanya ketika ada file yang diunggah
        if ($request->hasFile('foto_kejadian')) {
            $request->validate([
                'foto_kejadian' => 'required|mimes:jpg,jpeg,png,bmp,tiff'
            ]);
            $file = $request->file('foto_kejadian');
            $imageName = time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('image'), $imageName);
            $pelanggaran->foto_kejadian = $imageName;
        }
        $save = $pelanggaran->update([
            'tanggal_kejadian' => $request->tanggal_kejadian,
            'jenis_pelanggaran' => $request->jenis_pelanggaran,
            'nama_pelanggaran' => $request->nama_pelanggaran,
            'lokasi_pelanggaran' => $request->lokasi_pelanggaran,
            'sangsi_pelanggaran' => $request->sangsi_pelanggaran,
            'berita' => $validateData['excerpt'],
        ]);

        if ($save){
            // $dtpesan = tambahdt::where('id', $jenis->id)->first();
            // dd($dtpesan);
            return redirect()->route('home');
        }
       

    }
    public function hapusjenispl($id)
    {
        $data = tambahdt::findorfail($id);
        $data->delete();
        return back();
    }
    

    public function loginRegisterAjax(Request $request)
    {
        $jenis=jenispelanggaran::all();
        if ($request->input('option') === 'login') {
            $form = view('login-form')->render();
        } else {
            $form = view('register-form')->render();
        }

        return view('pengaduan',compact('jenis','form'));
    }

   

    public function update(Request $request, $id)
    {
        // Ambil data pengaduan berdasarkan id
        $pengaduan = Pengaduan::findOrFail($id);

        // Update status pengaduan
        $pengaduan->status = $request->status;
        $pengaduan->save();

        // Kirim email ke pelapor
        $data = [
            'nama_pelapor' => $pengaduan->nama_pelapor,
            'status' => $pengaduan->status,
            'uraian_lengkap_kejadian' => $pengaduan->uraian_lengkap_kejadian,
        ];
        Mail::to($pengaduan->email->email)->send(new PengaduanUpdated($data));

        return redirect()->back()->with('success', 'Status pengaduan berhasil diupdate');
    }

    public function hapuspengaduan($id)
    {
        $data = Pengaduan::findorfail($id);
        $data->delete();
        return back();
    }


    public function dashadmin()
    {
        $jenis = DB::table('tambahdts')
                ->join('jenispelanggarans', 'tambahdts.jenis_pelanggaran', '=', 'jenispelanggarans.id')
                ->select('jenispelanggarans.jenispelanggaran', DB::raw('COALESCE(COUNT(*), 0) as total'))
                ->groupBy('jenispelanggarans.jenispelanggaran')
                ->get();

        $data = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%m") as bulan'), DB::raw('count(*) as jumlah'))
                ->groupBy('bulan')
                ->pluck('jumlah');

        $bulan = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M") as bulan'))
                ->groupBy(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M")'))
                ->pluck('bulan');

        return view('admin/dashadmin', compact('jenis','data','bulan'));
    }

    public function homeadmin()
    {
        
            $jenis = jenispelanggaran::all();
            return view('admin/home', compact('jenis'));
       
    }

    public function dtpengaduan () {
        $pengaduan = pengaduan::all();
        // dd($pengaduan);
        return view('admin/dtpengaduan', compact('pengaduan'));
    }

    public function tambahdt () {
        $jenis=jenispelanggaran::all();
        return view('admin/tambahdt', compact('jenis'));
    }

    public function dashboard() {
        $jenis = jenispelanggaran::all();
        $pengaduan = pengaduan::all();
        $jumlahDiterima = pengaduan::where('status', 'diterima')->count();
        $jumlahDitolak = pengaduan::where('status', 'ditolak')->count();
        $jumlahDiproses = pengaduan::where('status', 'diproses')->count();
        $data = tambahdt::all();

        $jenis1 = DB::table('tambahdts')
                    ->join('jenispelanggarans', 'tambahdts.jenis_pelanggaran', '=', 'jenispelanggarans.id')
                    ->select('jenispelanggarans.jenispelanggaran', DB::raw('COALESCE(COUNT(*), 0) as total'))
                    ->groupBy('jenispelanggarans.jenispelanggaran')
                    ->get();

                $jenis1 = json_encode($jenis1);

        $jumlah = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%m") as bulan'), DB::raw('count(*) as jumlah'))
                            ->groupBy('bulan')
                            ->pluck('jumlah');
    
        $bulan = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M") as bulan'))
                            ->groupBy(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M")'))
                            ->pluck('bulan');

                        
        // dd($jenis1);
        return view('dashboard',compact('jenis','pengaduan','data','jumlahDiterima','jumlahDitolak','jumlahDiproses','jumlah','bulan','jenis1'));
    }

    public function tentangwbs() {
        return view('tentangwbs');
    }

    public function dasarhukum() {
        return view('dasarhukum');
    }

    public function alurpengaduan() {
        return view('alurpengaduan');
    }

    public function tatacara() {
        return view('tatacara');
    }

    public function faq() {
        return view('faq');
    }

    public function dtpelanggaran() {
        $jenis=jenispelanggaran::all();
        return view('dtpelanggaran', compact('jenis'));
    }

    public function pengaduan() {
        $jenis=jenispelanggaran::all();
        return view('pengaduan',compact('jenis'));
    }
    public function homeuser() {
        return view('homeuser');
    }
    public function master1() {
        $jenis = jenispelanggaran::all();
        $pengaduan = pengaduan::all();
        $jumlahDiterima = pengaduan::where('status', 'diterima')->count();
        $jumlahDitolak = pengaduan::where('status', 'ditolak')->count();
        $jumlahDiproses = pengaduan::where('status', 'diproses')->count();
        $data = tambahdt::all();

        $jenis1 = DB::table('tambahdts')
                    ->join('jenispelanggarans', 'tambahdts.jenis_pelanggaran', '=', 'jenispelanggarans.id')
                    ->select('jenispelanggarans.jenispelanggaran', DB::raw('COALESCE(COUNT(*), 0) as total'))
                    ->groupBy('jenispelanggarans.jenispelanggaran')
                    ->get();

                $jenis1 = json_encode($jenis1);

        $jumlah = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%m") as bulan'), DB::raw('count(*) as jumlah'))
                            ->groupBy('bulan')
                            ->pluck('jumlah');
    
        $bulan = tambahdt::select(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M") as bulan'))
                            ->groupBy(DB::raw('DATE_FORMAT(tanggal_kejadian, "%M")'))
                            ->pluck('bulan');

                        
        // dd($jenis1);
        return view('master1',compact('jenis','pengaduan','data','jumlahDiterima','jumlahDitolak','jumlahDiproses','jumlah','bulan','jenis1'));
    }

    
    

}