<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use App\Models\jenispelanggaran;
use Illuminate\Database\Console\Seeds\WhitoutModelEvents;
use Illuminate\Database\Seeder;

class JenisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        jenispelanggaran::create([
            'jenispelanggaran'=>'Perampokan dan Perompakan Bersenjata',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Tindakan-tindakan Terorisme',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Penyelundupan Senjata dan Senjata Pemusnah Massal',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Penyelundupan Obat-obat Terlarang',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Penyelundupan dan Perdagangan Manusia Lewat Laut',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Usaha Perikanan Ilegal Tidak Diatur dan Dilaporkan',    
        ]);
        jenispelanggaran::create([
            'jenispelanggaran'=>'Pengrusakan Terhadap Lingkungan Laut yang Disengaja',    
        ]);
    }
}
