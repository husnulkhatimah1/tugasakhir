<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTambahdtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tambahdts', function (Blueprint $table) {
            $table->id();
            $table->string("tanggal_kejadian");
            $table->string("foto_kejadian");
            $table->unsignedBigInteger("jenis_pelanggaran");
            $table->string("nama_pelanggaran");
            $table->string("lokasi_pelanggaran");
            $table->text("berita");
            $table->timestamps();
            $table->foreign("jenis_pelanggaran")->on('jenispelanggarans')->references('id')->onUpdate('cascade')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tambahdts');
    }
}
