<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengaduansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('email_id');
            $table->string("nama_pelapor");
            $table->string("unit_kerja_pelapor");
            $table->string("jabatan_pelapor");
            $table->string("tanggal_kejadian");
            $table->string("uraian_lengkap_kejadian");
            $table->string("dugaan_jenis_pelanggaran");
            $table->string("waktu_perkiraan_kejadian");
            $table->string('lokasi1'); 
            $table->string('status')->default('pending');
            $table->timestamps();
            $table->foreign("email_id")->on('users')->references('id')->onUpdate('cascade')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduans');
    }
}
