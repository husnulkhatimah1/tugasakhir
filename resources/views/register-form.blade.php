@if(session('message'))
      <div class="alert alert-success">
        {{session('message')}}
      </div>
      @endif
<div class="card">
    <div class="card-header">
        Data Pribadi
    </div>
    <div class="card-body">
        <form class="row g-3" action="{{route('register')}}" method="post" >
        @csrf
            <div class="col-auto">
                <label>Nama</label>
                <input type="text" name="name" class="form-control" placeholder="Nama" required="">
            </div>
            <div class="col-auto">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email" required="">
            </div>
            <div class="col-auto">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <div class="form-group">
                <label><i class="fa fa-address-book"></i> Role</label>
                <input type="text" name="role" class="form-control" value="User" readonly>
            </div>
            <div class="col-auto">
                <label></label>
                <button type="submit" class="btn btn-primary btn-block mt-2" >Register</button>
            </div>
            
            <hr>
        </form>
    </div>
</div>
