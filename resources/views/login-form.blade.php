<div class="card">
    <div class="card-header">
        Data Pribadi
    </div>
    <div class="card-body">
        <form class="row g-3" action="{{ route('actionlogin') }}" method="POST" >
        @csrf
            <div class="col-auto">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email" required="">
            </div>
            <div class="col-auto">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <div class="col-auto">
                <label></label>
                <button type="submit" class="btn btn-primary btn-block mt-2" >Sign in</button>
            </div>
            <hr>
        </form>
    </div>
</div>
