<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    {{-- <link rel="stylesheet" href="/css/style.css"> --}}
  </head>
  <style>
    a.text-black {
        text-decoration: none;
    }
    a.text-white {
        text-decoration: none;
    }
    .card-header1 {    
        width: 100%; /* Ubah lebar sesuai kebutuhan */
        background-color: rgba(0, 0, 0, 0.7);
        height: 6cm;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
        padding: 20px;
      }
    
</style>
  <body>
    {{-- Navbar --}}
    <div class="bg-light">
        <nav class="container navbar">
              <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                  <img src="{{asset('image/logo.png')}}" alt="Logo" width="100" height="60">
                </a>
                <div>
                  <p class="m-0 fs-5" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"><b>BADAN KEAMANAN LAUT</b></p>
                  <p class="m-0 fs-6" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">REPUBLIK INDONESIA</p>
                </div>
              </div>
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link text-danger" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif" aria-current="page" href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger  dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Informasi</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('tentangwbs')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tentang SIPP</a></li>
                      <li><a class="dropdown-item" href="{{route ('dasarhukum')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a></li>
                      <li><a class="dropdown-item" href="{{route ('dtpelanggaran')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pelanggaran</a></li>
                      <li><a class="dropdown-item" href="{{route ('alurpengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Alur Pengaduan</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li><a class="dropdown-item" href="{{route ('tatacara')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="color: #07162b;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item  nav-item active bg-danger text-white" href="{{route ('faq')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pengaduan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('pengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Buat Pengaduan</a></li>
                    </ul>
                </li>
              </ul>
        </nav>
    </div>

    {{-- konten --}}
    
    <div class="container-fluid p-0">
        <div class="text-white" style="background-image: url({{asset('image/abc.jpg')}})">
          <div class="card-header1 text-center py-5">
            <h1 class="mt-4" style="font-family: sans-serif"><b>Bantuan</b></h1>
          </div>
        </div>
    </div>

    <div class="container">
      <div class="card shadow p-3 mb-5 mt-5 bg-body rounded">
        <div class="card-header text-center" style="font-size: 20px;background-color: #07162b">
           <a class="text-white"> BANTUAN</a>
        </div>
        <div class="card-body">
          <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="flush-headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                  Apa itu Sistem Informasi Pelaporan Pelanggaran (SIPP) ?
                </button>
              </h2>
              <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Sistem Informasi Pelaporan Pelanggaran (SIPP) adalah aplikasi yang disediakan oleh Badan Keamanan Laut RI (Bakamla) untuk melaporkan suatu perbuatan berindikasi pelanggaran yang terjadi di lingkungan Bakamla. Bakamla menghargai informasi yang Anda laporkan karena fokus Kami adalah materi informasi yang Anda laporkan.</div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="flush-headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                  Jenis Pelanggaran Apa Saja Yang Dapat Saya Laporkan ?
                </button>
              </h2>
              <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Jenis pelanggaran yang dapat Anda laporkan, antara lain: Perompakan dan Perampokan, Usaha Perikanan Ilegal, Penyelundupan Obat-obat Terlarang, dan Melanggar Peraturan dan Perundangan yang Berlaku.</div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                  Bagaimana Dengan Kerahasiaan Data Diri Saya Sebagai Pelapor ?
                </button>
              </h2>
              <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Kami akan merahasiakan identitas pribadi Anda sebagai pelapor karena Kami hanya fokus pada informasi yang Anda laporkan. Kami sangat berhati-hati dalam penanganan pengaduan dengan menjaga kerahasiaan identitas Pelapor dan kerahasiaan materi pengaduan. Jangan memberitahukan/mengisikan data-data pribadi, seperti nama Anda, atau hubungan Anda dengan pelaku-pelaku. Jangan memberitahukan/mengisikan data-data/informasi yang memungkinkan bagi orang lain untuk melakukan pelacakan siapa Anda. Hindari orang lain mengetahui nama samaran (username), kata sandi (password), dan jawaban dari pertanyaan keamaman Anda.</div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="flush-headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                  Apa Hak-Hak Yang Saya Dapatkan Sebagai Pelapor ?
                </button>
              </h2>
              <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Anda mendapatkan jaminan perlindungan kerahasiaan identitas. Anda juga berkesempatan untuk dapat memberikan keterangan secara bebas tanpa paksaan dari pihak manapun. Selain itu, Anda mendapatkan informasi mengenai tahapan pengaduan yang telah Anda laporkan. Anda dapat mengajukan bukti untuk memperkuat pengaduan Anda. Serta Anda dapat berkomunikasi dengan tim verifikator dan investigator untuk bertanya ataupun mendapatkan kejelasan terkait Pengaduan Anda.</div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="flush-headingFive">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                  Bagaimana Tahapan Proses Pengaduan Yang Saya Laporkan ?
                </button>
              </h2>
              <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                <div class="accordion-body">Terdapat 4 (empat) tahapan status dalam penanganan pengaduan melalui SIPP Bakamla, yaitu:</p><ol><li><strong>OPEN, </strong>yaitu status ketika Anda telah membuat pengaduan dan belum diproses oleh tim Verifikasi.</li><li><strong>IN PROGRESS,</strong> yaitu status ketika pengaduan Anda dianggap telah memenuhi syarat dan mendapat respon dari verifikator SIPP Bakamla dan pengaduan siap untuk diproses (ditindak lanjuti) oleh Tim Investigator.</li><li><strong>DITERIMA, </strong>yaitu status pengaduan yang telah memenuhi syarat sesuai dengan ketentuan yang telah ditetapkan &nbsp;dan telah disetujui oleh Tim Investigator dengan memberikan rekomendasi.</li><li><strong>DITOLAK,</strong> yaitu status pengaduan yang <strong>tidak memenuhi syarat</strong> yang telah ditetapkan dalam aplikasi. Pengaduan dapat ditolak oleh Verifikator atau oleh Tim Investigator.</li></ol></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- footer --}}

    <footer class="text-white py-3" style="background-color: #07162b">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <p><b> BADAN KEAMANAN LAUT REPUBLIK INDONESIA</b></p>
            <p>Badan Keamanan Laut Republik Indonesia adalah badan yang bertugas melakukan patroli keamanan dan keselamatan
              di wilayah perairan Indonesia dan wilayah yurisdiksi Indonesia.</p>
            <hr>
            <p class="marker-address"><i class="fas fa-map-marker-alt fa-fw"></i> Bakamla Gedung Perintis Kemerdekaan</p>
            <p class="address">Jl. Proklamasi No.56, RT.10/RW.2, Pegangsaan, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10320</p>
          </div>
          <div class="col-lg-6 text-lg-end">
            <p>Hubungi Kami</p>
            <p><a href="https://wa.link/6fszq3" class="text-white">Whatsapp<i class="fab fa-whatsapp ms-3" style="font-size:6mm"></i></a>
            <p><a href="tel:+622150858130" class="text-white"> (021) 5084 8130<i class="fas fa-phone-alt fa-fw ms-3"></i></a></p>
            <p><a href="mailto:humas@bakamla.go.id" class="text-white">humas@bakamla.go.id<i class="fas fa-envelope fa-fw ms-3"></i></a></p>
            <p>&copy; 2023 Badan Keamanan Laut Indonesia. All rights reserved.</p>
          </div>
        </div>
      </div>
  </footer>      
    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>

