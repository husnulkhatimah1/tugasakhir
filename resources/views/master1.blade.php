<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="/css/style.css">
  </head>
  <style>
    a.text-black {
        text-decoration: none;
    }
    a.text-white {
        text-decoration: none;
    }
    
</style>
  <body>
    {{-- Navbar --}}
    <div class="bg-light">
        <nav class="container navbar">
              <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                  <img src="{{asset('image/logo.png')}}" alt="Logo" width="100" height="60">
                </a>
                <div>
                  <p class="m-0 fs-5" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"><b>BADAN KEAMANAN LAUT</b></p>
                  <p class="m-0 fs-6" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">REPUBLIK INDONESIA</p>
                </div>
              </div>
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link active" style="color: #07162b;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif" aria-current="page" href="#">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Informasi</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tentang SIPP</a></li>
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a></li>
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pelanggaran</a></li>
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Alur Pengaduan</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Bantuan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pengaduan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="#" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Buat Pengaduan</a></li>
                    </ul>
                </li>
              </ul>
        </nav>
    </div>

    {{-- konten --}}
    
    <div class="container-fluid p-0">
        <div class="text-white" style="background-image: url({{asset('image/abc.jpg')}})">
          <div class="card-header text-center py-5">
            <h1 class="mt-4" style="font-family: sans-serif"><b>Dashboard</b></h1>
          </div>
        </div>
    </div>
    <div class="container-fluid mb-5">
        <div class="row mr-2 mt-2 justify-content-center">
            <div class="col-lg-3 col-sm-6 col-12">
              <div class="card shadow p-2 mb-4 mt-3 bg-body rounded rounded-pill">
                <div class="card rounded-pill" style="background-color:black;color:white ">
                    <div class="row g-0">
                      <div class="col-md-4 text-center align-self-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-database-fill" viewBox="0 0 16 16">
                            <path d="M3.904 1.777C4.978 1.289 6.427 1 8 1s3.022.289 4.096.777C13.125 2.245 14 2.993 14 4s-.875 1.755-1.904 2.223C11.022 6.711 9.573 7 8 7s-3.022-.289-4.096-.777C2.875 5.755 2 5.007 2 4s.875-1.755 1.904-2.223Z"/>
                            <path d="M2 6.161V7c0 1.007.875 1.755 1.904 2.223C4.978 9.71 6.427 10 8 10s3.022-.289 4.096-.777C13.125 8.755 14 8.007 14 7v-.839c-.457.432-1.004.751-1.49.972C11.278 7.693 9.682 8 8 8s-3.278-.307-4.51-.867c-.486-.22-1.033-.54-1.49-.972Z"/>
                            <path d="M2 9.161V10c0 1.007.875 1.755 1.904 2.223C4.978 12.711 6.427 13 8 13s3.022-.289 4.096-.777C13.125 11.755 14 11.007 14 10v-.839c-.457.432-1.004.751-1.49.972-1.232.56-2.828.867-4.51.867s-3.278-.307-4.51-.867c-.486-.22-1.033-.54-1.49-.972Z"/>
                            <path d="M2 12.161V13c0 1.007.875 1.755 1.904 2.223C4.978 15.711 6.427 16 8 16s3.022-.289 4.096-.777C13.125 14.755 14 14.007 14 13v-.839c-.457.432-1.004.751-1.49.972-1.232.56-2.828.867-4.51.867s-3.278-.307-4.51-.867c-.486-.22-1.033-.54-1.49-.972Z"/>
                          </svg>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Pelanggaran</h5>
                          <p class="card-text">Jumlah: {{$data->count()}}</p>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
              <div class="card shadow p-2 mb-4 mt-3 bg-body rounded rounded-pill">
                <div class="card rounded-pill" style="background-color:rgb(146, 72, 19);color:white ">
                    <div class="row g-0">
                      <div class="col-md-4 align-self-center text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-clipboard-data-fill" viewBox="0 0 16 16">
                            <path d="M6.5 0A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3Zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3Z"/>
                            <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5v-1ZM10 8a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V8Zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1Zm4-3a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0v-3a1 1 0 0 1 1-1Z"/>
                          </svg>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Jenis Pelanggaran</h5>
                          <p class="card-text">Jumlah: {{$jenis->count()}}</p>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
              <div class="card shadow p-2 mb-4 mt-3 bg-body rounded rounded-pill">
                <div class="card rounded-pill" style="background-color:rgb(15, 15, 68);color:white ">
                    <div class="row g-0">
                      <div class="col-md-4 text-center align-self-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-file-bar-graph-fill" viewBox="0 0 16 16">
                            <path d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm-2 11.5v-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-2.5.5a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1zm-3 0a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-1z"/>
                          </svg>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title">Pengaduan</h5>
                          <p class="card-text">Jumlah: {{$pengaduan->count()}}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="row mr-2 mt-2 justify-content-center">
            <div class="col-lg-5 col-sm-6 col-12 mt-3">
                <div class="card shadow mb-2 ms-3 rounded">
                  <div class="card-body">
                    <div class="container">
                      <div id="grafik"></div>
                    </div>
                  </div>
                </div>
            </div>
          
            <div class="col-lg-5 col-sm-6 col-12 mt-3">
                <div class="card shadow mb-2 ms-3 rounded">
                  <div class="card-body">
                    <div class="container">
                      <div id="pie"></div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="col-lg-5 col-sm-6 col-12 mt-4">
                <div class="card shadow mb-2 ms-3 rounded">
                  <div class="card-body">
                    <div class="container">
                      <div id="batang"></div>
                    </div>
                  </div>
                </div>
            </div>

          </div>
        </div>
          
          <script src="https://code.highcharts.com/highcharts.js"></script>
          <script type="text/javascript">
              var jumlah = <?php echo json_encode($jumlah) ?>;
              var bulan = <?php echo json_encode($bulan) ?>;

              var jenis1 = <?php echo $jenis1 ?>;
          
              var terima = <?php echo json_encode($jumlahDiterima) ?>;
              var tolak = <?php echo json_encode($jumlahDitolak) ?>;
              var proses = <?php echo json_encode($jumlahDiproses) ?>;
              
          
              Highcharts.chart('grafik', {
                title : {
                  text : 'Grafik Bulanan',
                  style: {
                    color: '#07162b', // Warna teks yang diinginkan
                    fontWeight: 'bold' // Ketebalan teks yang diinginkan
                    }
                },
                xAxis : {
                  categories : bulan
                },
                yAxis : {
                  title : {
                    text : 'Jumlah Pelanggaran Bulanan',
                    style: {
                    color: 'red', // Warna teks yang diinginkan
                    }
                  }
                },
                plotOptions: {
                  series : {
                    allowPointSelect: true
                  }
                },
                series:[
                  {
                    name: 'Bulan',
                    data: jumlah
                  }
                ]
              });
          
              //chart_pie
          
              Highcharts.chart('pie', {
              chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
              },
              title: {
                  text: 'Grafik Berdasarkan Pengaduan',
                  style: {
                    color: '#07162b', // Warna teks yang diinginkan
                    fontWeight: 'bold' // Ketebalan teks yang diinginkan
                    },
                  align: 'left'
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              accessibility: {
                  point: {
                      valueSuffix: '%'
                  }
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.y}'
                      }
                  }
              },
          
              series: [{
                  name: 'Brands',
                  colorByPoint: true,
                  data: [{
                      name: 'Diterima',
                      y: terima,
                      sliced: true,
                      selected: true
                  },
                  {
                      name: 'Ditolak',
                      y: tolak,
                  },
                  {
                    name: 'Diproses',
                      y: proses,
                  }
                ]
              }]
              });
              
            //   chart batang
            Highcharts.chart('batang', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Average Rainfall'
                },
                subtitle: {
                    text: 'Source: WorldClimate.com'
                },
                xAxis: {
                    categories: jenis1.map(item => item.jenispelanggaran),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                series: [{
                    name: 'Jenis Pelanggaran',
                    data: jenis1.map(item => item.total),
                }]
            });
                

          </script>
        


    {{-- footer --}}

    <footer class="text-white py-3" style="background-color: #07162b">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <p><b> BADAN KEAMANAN LAUT REPUBLIK INDONESIA</b></p>
              <p>Badan Keamanan Laut Republik Indonesia adalah badan yang bertugas melakukan patroli keamanan dan keselamatan
                di wilayah perairan Indonesia dan wilayah yurisdiksi Indonesia.</p>
              <hr>
              <p class="marker-address"><i class="fas fa-map-marker-alt fa-fw"></i> Bakamla Gedung Perintis Kemerdekaan</p>
              <p class="address">Jl. Proklamasi No.56, RT.10/RW.2, Pegangsaan, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10320</p>
            </div>
            <div class="col-lg-6 text-lg-end">
              <p>Hubungi Kami</p>
              <p><a href="https://wa.link/6fszq3" class="text-white">Whatsapp<i class="fab fa-whatsapp ms-3" style="font-size:6mm"></i></a>
              <p><a href="tel:+622150858130" class="text-white"> (021) 5084 8130<i class="fas fa-phone-alt fa-fw ms-3"></i></a></p>
              <p><a href="mailto:humas@bakamla.go.id" class="text-white">humas@bakamla.go.id<i class="fas fa-envelope fa-fw ms-3"></i></a></p>
              <p>&copy; 2023 Badan Keamanan Laut Indonesia. All rights reserved.</p>
            </div>
          </div>
        </div>
    </footer>      
      
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>