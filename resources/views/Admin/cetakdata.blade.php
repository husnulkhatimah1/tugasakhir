<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table.static{
            position: relative;
            border: 1px solid #543535;
        }
    </style>
</head>
<body>
    <div class="form-group">
        <p align="center"><b>Data Pelanggaran</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width: 95%">
            <tr>
                <th scope="col" >No</th>
                <th scope="col">Tanggal Kejadian</th>
                <th scope="col">Foto Pelanggaran</th>
                <th scope="col">Nama Pelanggaran</th>
                <th scope="col">Lokasi Pelanggaran</th>
                <th scope="col">Jenis Pelanggaran</th>
            </tr>
            @foreach ($pelanggaran as $item)
              <tr class="text-center">
                  <td>{{$loop->iteration }}</td>
                  <td>{{$item->tanggal_kejadian}}</td>
                  <td><img src="{{asset('image') . '/' .$item->foto_kejadian }}" class="rounded" style="width: 150px"></td>
                  <td>{{$item->nama_pelanggaran}}</td>
                  <td>{{$item->lokasi_pelanggaran}}</td>
                  <td>{{$item->jenis_pelanggaran}}</td>
                  
                </tr>
            @endforeach

        </table>
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>
</html>