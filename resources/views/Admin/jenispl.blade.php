@extends('Admin/master')
@section('konten')

<div class="container pt-2">
  <div class="row justify-content-center">
    <div class="col-sm-15 ">
      <div class="card  shadow p-4 mb-5 bg-body rounded">
        <div class="card-body ">
          <div class="d-flex justify-content-between">
            <a class="btn text-white mb-3 align-self-center " style="background-color: #07162b" href="{{route('homeadmin')}}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-rewind-fill" viewBox="0 0 16 16">
              <path d="M8.404 7.304a.802.802 0 0 0 0 1.392l6.363 3.692c.52.302 1.233-.043 1.233-.696V4.308c0-.653-.713-.998-1.233-.696L8.404 7.304Z"/>
              <path d="M.404 7.304a.802.802 0 0 0 0 1.392l6.363 3.692c.52.302 1.233-.043 1.233-.696V4.308c0-.653-.713-.998-1.233-.696L.404 7.304Z"/>
            </svg></a>
            <a class="btn text-white mb-3 align-self-center " style="background-color: #07162b" target="_blank" href="{{route('cetakdata')}}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer-fill" viewBox="0 0 16 16">
              <path d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5zm6 8H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
              <path d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
            </svg></a>
          </div>
          <table class="table table-sm table-hover" >
            <thead>
              <tr style="background-color: #07162b" class="text-white text-center">
                <th scope="col" >No</th>
                <th scope="col">Tanggal Kejadian</th>
                <th scope="col">Foto Pelanggaran</th>
                <th scope="col">Nama Pelanggaran</th>
                <th scope="col">Lokasi Pelanggaran</th>
                <th scope="col">Pilihan</th>
              </tr>
            </thead>
              @foreach ($pelanggaran as $item)
              <tr class="text-center">
                  <td>{{$loop->iteration }}</td>
                  <td>{{$item->tanggal_kejadian}}</td>
                  <td><img src="{{asset('image') . '/' .$item->foto_kejadian }}" class="rounded" style="width: 150px"></td>
                  <td>{{$item->nama_pelanggaran}}</td>
                  <td>{{$item->lokasi_pelanggaran}}</td>
                  <td>
                    <a href="{{route('editjenispl',$item->id)}}"><button class="btn btn-warning"><svg class="bi pe-none " width="15" height="15"><use xlink:href="#pen"/></svg></button></a>
                    <a href="{{route('hapusjenispl',$item->id)}}"><button class="btn btn-danger"><svg class="bi pe-none " width="15" height="15"><use xlink:href="#sampah"/></svg></button></a>
                  </td>
                </tr>
              @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
        
