@extends('Admin/master')

@section('konten')

<div class="container"><br>
    {{-- <a href="{{ URL::previous() }}" class="btn btn-primary">Kembali</a> --}}

    <div class="align-bottom">
        <h2 class="text-center">FORM PENGADUAN</h3>
        <hr>
        @if(session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
        @endif
        
        
        <form action="{{route('simpaneditpl', $pelanggaran->id)}}" method="post" class="p-3" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label><i class="fa fa-calendar"></i> Tanggal Kejadian </label>
                <input type="date" name="tanggal_kejadian" class="form-control" placeholder="Tanggal Kejadian" value="{{$pelanggaran->tanggal_kejadian}}">
            </div>
            <div class="form-group">
                <label><i class="fa fa-camera"></i> Foto Kejadian </label>
                <input type="file" name="foto_kejadian" class="form-control" placeholder="Foto Kejadian" value="{{$pelanggaran->foto_kejadian}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="fa fa-edit">Jenis Pelanggaran</label>
                <!-- gunakan event onchange untuk mengirim data secara otomatis  -->
                <select class="form-control" name="jenis_pelanggaran">
                    @foreach ($jenis as $item)     
                        <option value="{{$item->id}}"@if($item->id==$pelanggaran->jenis_pelanggaran)selected @endif>{{$item->jenispelanggaran}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label><i class="fa fa-map-marker"></i> Nama Pelanggaran </label>
                <input type="text" name="nama_pelanggaran" class="form-control" placeholder="Nama Pelanggaran"  value="{{$pelanggaran->nama_pelanggaran}}">
            </div>
            <div class="form-group">
                <label><i class="fa fa-map-marker"></i> Lokasi Pelanggaran </label>
                <input type="text" name="lokasi_pelanggaran" class="form-control" placeholder="Lokasi Pelanggaran"  value="{{$pelanggaran->lokasi_pelanggaran}}">
            </div>
            <div class="form-group">
                <label><i class="fa fa-anchor"></i> Berita </label>
                <input id="berita" type="hidden" name="berita" value="{{$pelanggaran->berita}}">
                <trix-editor input="berita" class="trix-content" data-trix-value="{{$pelanggaran->berita}}"></trix-editor>
            </div>
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user"></i> SIMPAN</button>
            <hr>
        </form>
        
        <script>
            document.addEventListener('trix-file-accept', function(e){
                e.preventDefault();
            })
        </script>
        @endsection
  