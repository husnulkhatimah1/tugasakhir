<?php
if(isset($_FILES["foto_kejadian"])) {
    $nama_file = $_FILES["foto_kejadian"]["name"];
    $ukuran_file = $_FILES["foto_kejadian"]["size"];
    $tmp_file = $_FILES["foto_kejadian"]["tmp_name"];
    $error = $_FILES["foto_kejadian"]["error"];

    // Periksa apakah file yang diunggah adalah gambar
    $ekstensi_diperbolehkan = array("jpg", "jpeg", "png", "gif");
    $ekstensi_file = strtolower(pathinfo($nama_file, PATHINFO_EXTENSION));
    if(!in_array($ekstensi_file, $ekstensi_diperbolehkan)) {
        echo "Error: Hanya file gambar yang diperbolehkan.";
        exit;
    }

    // Periksa ukuran file
    $ukuran_diperbolehkan = 1024 * 1024; // 1 MB
    if($ukuran_file > $ukuran_diperbolehkan) {
        echo "Error: Ukuran file terlalu besar. Maksimum 1 MB.";
        exit;
    }

    // Pindahkan file ke direktori yang diinginkan
    $direktori_upload = "public/";
    $nama_file_baru = uniqid() . '.' . $ekstensi_file;
    $file_upload = move_uploaded_file($tmp_file, __DIR__ . '/../../public' . $nama_file_baru);

    // Cek apakah file berhasil diupload
    if($file_upload) {
        echo "File berhasil diupload ke " . $direktori_upload . $nama_file_baru;
    } else {
        echo "Error: File gagal diupload.";
    }
}
?>
