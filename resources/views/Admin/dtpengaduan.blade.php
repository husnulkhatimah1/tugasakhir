@extends('Admin/master')
@section('konten')

<div class="container pt-4">
  <div class="row justify-content-center">
    <div class="col-sm-15 mt-3">
      <div class="card shadow p-3 mb-4 rounded  ">
        <div class="card-body ">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr style="background-color: #07162b" class="text-white text-center">
                  <th scope="col">No</th>
                  <th scope="col">Email</th>
                  <th scope="col">Nama Pelapor</th>
                  <th scope="col">Unit Kerja Pelapor</th>
                  <th scope="col">Jabatan Pelapor</th>
                  <th scope="col">Tanggal Kejadian</th>
                  <th scope="col">Uraian Lengkap Kejadian</th>
                  <th scope="col">Dugaan Jenis Pelanggaran</th>
                  <th scope="col">Waktu Perkiraan Kejadian</th>
                  <th scope="col">Lokasi</th>
                  <th scope="col">Status</th>
                  <th scope="col">Pilihan</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($pengaduan as $item)
                <tr class="text-center">
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $item->email->email }}</td>
                  <td>{{ $item->nama_pelapor }}</td>
                  <td>{{ $item->unit_kerja_pelapor }}</td>
                  <td>{{ $item->jabatan_pelapor }}</td>
                  <td>{{ $item->tanggal_kejadian }}</td>
                  <td>{{ $item->uraian_lengkap_kejadian }}</td>
                  <td>{{ $item->dugaan_jenis_pelanggaran }}</td>
                  <td>{{ $item->waktu_perkiraan_kejadian }}</td>
                  <td>
                    <div id="map-{{ $loop->iteration }}" style="width: 400px; height: 300px;"></div>
                    <script>
                      function initMap{{ $loop->iteration }}() {
                        var lokasi = "{{ $item->lokasi1 }}";
                        var koordinat = lokasi.split(',');
                        var latLng = new google.maps.LatLng(parseFloat(koordinat[0]), parseFloat(koordinat[1]));
                        var mapOptions = {
                          center: latLng,
                          zoom: 13
                        };
                        var map = new google.maps.Map(document.getElementById('map-{{ $loop->iteration }}'), mapOptions);
                        var marker = new google.maps.Marker({
                          position: latLng,
                          map: map,
                          title: "{{ $item->nama_pelapor }}"
                        });
                      }
                    </script>
                  </td>
                  <td>{{ $item->status }}</td>
                  <td>
                    <form action="{{ route('pengaduan.update', ['id' => $item->id]) }}" method="POST">
                      @csrf
                      <input type="hidden" name="_method" value="PUT">
                      <button type="submit" class="btn btn-success mb-1" name="status" value="diterima">
                        <svg class="bi pe-none" width="15" height="15">
                          <use xlink:href="#terima"/>
                        </svg>
                      </button>
                      <button type="submit" class="btn btn-warning mb-1" name="status" value="ditolak">
                        <svg class="bi pe-none" width="15" height="15">
                          <use xlink:href="#tolak"/>
                        </svg>
                      </button>
                      <a href="{{ route('hapuspengaduan', $item->id) }}" class="btn btn-danger">
                        <svg class="bi pe-none" width="15" height="15">
                          <use xlink:href="#sampah"/>
                        </svg>
                      </a>
                    </form>
                  </td>
                </tr>
                
                @endforeach
                <script>
                  window.onload = function() {
                    @foreach ($pengaduan as $item)
                      initMap{{ $loop->iteration }}();
                    @endforeach
                  };
                </script>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
