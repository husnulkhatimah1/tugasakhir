@extends('Admin/master')
@section('konten')


<div class="container pt-4">
  <div class="row justify-content-center">
    @foreach ($jenis as $item)
    <div class="col-sm-5 mt-3">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <div class="card-body text-center">
          <h6 class="card-title mb-5">{{$item->jenispelanggaran}}</h6>
          <a href="{{route('jenispl',['id'=>$item->id])}}" class="btn text-white" style="background-color: #07162b">Selengkapnya</a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
  </div>

@endsection