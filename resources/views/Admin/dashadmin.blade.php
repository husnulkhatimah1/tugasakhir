@extends('Admin/master')
@section('konten')


<div class="container">
  <div class="row">
    @foreach ($jenis as $index => $item)
      @php
        $colors = ['#07162b', 'red', 'green', 'orange', 'black','navy','chocolate'];
        $icons = ['#laut', '#laut1', '#laut2', '#laut3', '#laut4','#laut5','#laut6'];
        $colorIndex = $index % count($colors);
        $colorClass = $colors[$colorIndex];
        $iconClass = $icons[$colorIndex];
      @endphp
      
      <div class="col-sm-4">
        <div class="card shadow mb-2 rounded" style="background-color:{{$colorClass}};color:white ">
          <div class="row g-0">
            <div class="col-md-4">
              <svg class="bi pe-none " width="" height=""><use xlink:href="{{ $iconClass }}" fill="#ffffff"/></svg>
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">{{ $item->jenispelanggaran }}</h5>
                <p class="card-text">Jumlah: {{ $item->total }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    
    
  </div>
</div>
<div class="card shadow mb-2 ms-3 rounded">
  <div class="card-body">
    <div class="container">
      <div id="grafik"></div>
    </div>
  </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
    var jumlah = <?php echo json_encode($data) ?>;
    var bulan = <?php echo json_encode($bulan) ?>;
    

    Highcharts.chart('grafik', {
      title : {
        text : 'Grafik Bulanan'
      },
      xAxis : {
        categories : bulan
      },
      yAxis : {
        title : {
          text : 'Jumlah Pelanggaran Bulanan'
        }
      },
      plotOptions: {
        series : {
          allowPointSelect: true
        }
      },
      series:[
        {
          name: 'jumlah pelanggaran',
          data: jumlah
        }
      ]
    });
</script>

        
        @endsection