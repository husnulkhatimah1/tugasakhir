@extends('Admin/master')

@section('konten')

<div class="container pt-4">
    <div class="row justify-content-center">
      <div class="col-sm-15 mt-3">
        <div class="card shadow p-3 mb-4 rounded  ">
          <div class="card-body ">
            <div class="row justify-content-center">
                <h2 class="text-center">FORM DATA PELANGGARAN</h3>
                <hr>
                @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
                @endif
                
                
                <form action="{{url ('tambah1')}}" method="post" class="p-3" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label><i class="fa fa-calendar"></i> Tanggal Kejadian </label>
                        <input type="date" name="tanggal_kejadian" class="form-control" placeholder="Tanggal Kejadian" required="">
                    </div>
                    <div class="form-group">
                        <label><i class="fa fa-camera"></i> Foto Kejadian </label>
                        <input type="file" name="foto_kejadian" class="form-control" placeholder="Foto Kejadian" >
                    </div>                
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="fa fa-edit">Jenis Pelanggaran</label>
                        <!-- gunakan event onchange untuk mengirim data secara otomatis  -->
                        <select class="form-control" name="jenis_pelanggaran">
                            @foreach ($jenis as $item)     
                                <option value="{{$item->id}}">{{$item->jenispelanggaran}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label><i class="fa fa-map-marker"></i> Nama Pelanggaran </label>
                        <input type="text" name="nama_pelanggaran" class="form-control" placeholder="Nama Pelanggaran" required="">
                    </div>
                    <div class="form-group">
                        <label><i class="fa fa-map-marker"></i> Lokasi Pelanggaran </label>
                        <input type="text" name="lokasi_pelanggaran" class="form-control" placeholder="Lokasi Pelanggaran" required="">
                    </div>
                    <div class="form-group">
                        <label><i class="fa fa-anchor"></i> Berita </label>
                        <input id="berita" type="hidden" name="berita">
                        <trix-editor input="berita" class="trix-content"></trix-editor>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block mt-3"><i class="fa fa-user"></i> SIMPAN</button>
                    <hr>
                    
                </form>
        
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
        <script>
            document.addEventListener('trix-file-accept', function(e){
                e.preventDefault();
            })
        </script>
        @endsection
  