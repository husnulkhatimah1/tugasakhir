<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
  </head>
  <body style="background-image: url({{asset('image/abc.jpg')}})">
    <div class="container d-flex text-white" style="margin-bottom: 90px">
      <div class="me-auto p-2 fs-1" style="font-size: 50px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: bold">SIPP</div>
      <div class="p-2"><a href="{{route ('login')}}" class="btn btn-danger fs-5" id="myButton" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif">Login</a></div>
    </div>
    <div class="container justify-content-center">
        <p class="text-white" style="font-size: 30px;font-family: Arial, Helvetica, sans-serif">SELAMAT DATANG DI</p>
        <p class="text-white" style="font-size: 50px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: bold">Sistem Informasi <br> Pelaporan Pelanggaran <br>(SIPP) BAKAMLA RI</p>
        <p class="text-white" style="font-size: 20px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Laporkan Setiap Pelanggaran Yang Terjadi <br> Di Laut Nusantara</p>
        <a id="myButton" class="btn btn-danger p-3 fs-5 mt-3" href="{{route('dashboard')}}" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif">Get Started</a>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script>
        const button = document.getElementById("myButton");
        button.addEventListener("mouseenter", function() {
          button.classList.add("btn-hover");
        });
        button.addEventListener("mouseleave", function() {
          button.classList.remove("btn-hover");
        });
      </script>
  </body>
</html>

{{-- <!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body style="background-image: url({{asset('image/abc.jpg')}})">
    <style>
      main {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100vh;
          text-align: center;
      }
  </style>
  
    <main role="main">
        <div class=" text-white">
          <h3 class="mt-5">SELAMAT DATANG DI</h3><br>
          <h1><b>Sistem Informasi Pelaporan Pelanggaran (SIPP) BAKAMLA RI</b></h1><br>
          <h5>Laporkan Setiap Pelanggaran Yang Terjadi Di Laut Nusantara</h5>
          <a class="btn btn-danger mt-3" href="{{route('dashboard')}}">Get Started --></a>
        </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html> --}}
