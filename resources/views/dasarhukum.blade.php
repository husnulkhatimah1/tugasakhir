<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="/css/style.css">
  </head>
  <style>
    a.text-black {
        text-decoration: none;
    }
    a.text-white {
        text-decoration: none;
    }
    
</style>
  <body>
    {{-- Navbar --}}
    <div class="bg-light">
        <nav class="container navbar">
              <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                  <img src="{{asset('image/logo.png')}}" alt="Logo" width="100" height="60">
                </a>
                <div>
                  <p class="m-0 fs-5" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"><b>BADAN KEAMANAN LAUT</b></p>
                  <p class="m-0 fs-6" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">REPUBLIK INDONESIA</p>
                </div>
              </div>
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link text-danger" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif" aria-current="page" href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="color: #07162b;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('tentangwbs')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tentang SIPP</a></li>
                      <li><a class="dropdown-item nav-item active bg-danger text-white" href="{{route ('dasarhukum')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a></li>
                      <li><a class="dropdown-item" href="{{route ('dtpelanggaran')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pelanggaran</a></li>
                      <li><a class="dropdown-item" href="{{route ('alurpengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Alur Pengaduan</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li><a class="dropdown-item" href="{{route ('tatacara')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Bantuan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('faq')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pengaduan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('pengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Buat Pengaduan</a></li>
                    </ul>
                </li>
              </ul>
        </nav>
    </div>

    {{-- konten --}}
    
    <div class="container-fluid p-0">
        <div class="text-white" style="background-image: url({{asset('image/abc.jpg')}})">
          <div class="card-header text-center py-5">
            <h1 class="mt-4" style="font-family: sans-serif"><b>Dasar Hukum</b></h1>
          </div>
        </div>
    </div> 

    <div class="container">
      <div class="card mb-3 p-2 mt-5 shadow p-3 mb-5 bg-body rounded" >
        <div class="row g-0">
          <div class="col-md-1">
            <img src="image/bakamla.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h6 class="card-title">Peraturan Presiden (PERPRES) Nomor 178 Tahun 2014. tentang Badan Keamanan Laut</h6>
              <a href="https://docs.google.com/document/d/e/2PACX-1vQTduOi0BMeAnm_FckZxdwvZ43PsBwzsqVsI6-Z2xNBtyl7VZySXGH6vXuJc19X0K5j6zpzNEySqET0/pub" class="btn btn-outline-danger">Lihat</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card shadow p-2 mb-5 bg-body rounded" >
        <div class="row g-0">
          <div class="col-md-1">
            <img src="image/bakamla.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h6 class="card-title">UU Nomor 31 Tahun 2004 Tentang Perikanan</h6>
              <a href="https://docs.google.com/document/d/e/2PACX-1vTvEIlRs6ociRnFI5GhaACojNh6Qbc7MkAD5jAdFGG4m4h9P945MYjN1BWHpT18g2waadmD0t7XVtn6/pub" class="btn btn-outline-danger">Lihat</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card shadow p-2 mb-5 bg-body rounded" >
        <div class="row g-0">
          <div class="col-md-1">
            <img src="image/bakamla.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h6 class="card-title">UU Nomor 6 Tahun 1996 Tentang Perairan Indonesia</h6>
              <a href="https://docs.google.com/document/d/e/2PACX-1vReDg0VHl-JKjcd5FPcsBgfYQD7aXPPwzfGvq7O1clquJQcQIbOILAuuYoMPS7_odk18_N-twhVFSCK/pub" class="btn btn-outline-danger">Lihat</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card shadow p-2 mb-5 bg-body rounded" >
        <div class="row g-0">
          <div class="col-md-1">
            <img src="image/bakamla.jpg" class="img-fluid rounded-start" alt="...">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h6 class="card-title">UU Nomor 32 Tahun 2014 Tentang Kelautan</h6>
              <a href="https://docs.google.com/document/d/e/2PACX-1vR_yyPQtU4U2Q5OSDstFhA0kkoqNNRvwni-l7HIgq891u3ZDZ-vLQKjz7F2aol8AJekseCEzSrcuvVS/pub" class="btn btn-outline-danger">Lihat</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- footer --}}

    <footer class="text-white py-3" style="background-color: #07162b">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <p><b> BADAN KEAMANAN LAUT REPUBLIK INDONESIA</b></p>
            <p>Badan Keamanan Laut Republik Indonesia adalah badan yang bertugas melakukan patroli keamanan dan keselamatan
              di wilayah perairan Indonesia dan wilayah yurisdiksi Indonesia.</p>
            <hr>
            <p class="marker-address"><i class="fas fa-map-marker-alt fa-fw"></i> Bakamla Gedung Perintis Kemerdekaan</p>
            <p class="address">Jl. Proklamasi No.56, RT.10/RW.2, Pegangsaan, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10320</p>
          </div>
          <div class="col-lg-6 text-lg-end">
            <p>Hubungi Kami</p>
            <p><a href="https://wa.link/6fszq3" class="text-white">Whatsapp<i class="fab fa-whatsapp ms-3" style="font-size:6mm"></i></a>
            <p><a href="tel:+622150858130" class="text-white"> (021) 5084 8130<i class="fas fa-phone-alt fa-fw ms-3"></i></a></p>
            <p><a href="mailto:humas@bakamla.go.id" class="text-white">humas@bakamla.go.id<i class="fas fa-envelope fa-fw ms-3"></i></a></p>
            <p>&copy; 2023 Badan Keamanan Laut Indonesia. All rights reserved.</p>
          </div>
        </div>
      </div>
  </footer>      
    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>

