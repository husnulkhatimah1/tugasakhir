<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    {{-- <link rel="stylesheet" href="/css/style.css"> --}}
  </head>
  <style>
    a.text-black {
        text-decoration: none;
    }
    a.text-white {
        text-decoration: none;
    }
    .card-header1 {    
        width: 100%; /* Ubah lebar sesuai kebutuhan */
        background-color: rgba(0, 0, 0, 0.7);
        height: 6cm;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
        padding: 20px;
      }
    
</style>
  <body>
    {{-- Navbar --}}
    <div class="bg-light">
        <nav class="container navbar">
              <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                  <img src="{{asset('image/logo.png')}}" alt="Logo" width="100" height="60">
                </a>
                <div>
                  <p class="m-0 fs-5" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"><b>BADAN KEAMANAN LAUT</b></p>
                  <p class="m-0 fs-6" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">REPUBLIK INDONESIA</p>
                </div>
              </div>
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link text-danger" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif" aria-current="page" href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="color: #07162b;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('tentangwbs')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tentang SIPP</a></li>
                      <li><a class="dropdown-item" href="{{route ('dasarhukum')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a></li>
                      <li><a class="dropdown-item" href="{{route ('dtpelanggaran')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pelanggaran</a></li>
                      <li><a class="dropdown-item" href="{{route ('alurpengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Alur Pengaduan</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li><a class="dropdown-item nav-item active bg-danger text-white" href="{{route ('tatacara')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Bantuan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('faq')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pengaduan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('pengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Buat Pengaduan</a></li>
                    </ul>
                </li>
              </ul>
        </nav>
    </div>

    {{-- konten --}}
    
    <div class="container-fluid p-0">
        <div class="text-white" style="background-image: url({{asset('image/abc.jpg')}})">
          <div class="card-header1 text-center py-5">
            <h1 class="mt-4" style="font-family: sans-serif"><b>Tata Cara</b></h1>
          </div>
        </div>
    </div>

    <div class="container">
      <div class="card shadow p-3 mb-5 mt-5 bg-body rounded">
        <div class="card-header text-center" style="font-size: 20px;background-color: #07162b">
          <b style="color: yellow">TATA CARA </b> <a class="text-white"> PENGADUAN</a>
        </div>
        <div class="card-body">
          <h4 style="font-family: 'Courier New', Courier, monospace; font-weight: bold">Berikut ini adalah langkah-langkah yang dilakukan untuk membuat pengaduan melalui aplikasi Sistem Informasi Pelaporan Pelanggaran (SIPP) Bakamla, yaitu:</h4>
          <p >&nbsp;</p><ol style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif"><li>Sebelum melaporkan pengaduan Anda di Sistem Informasi Pelaporan Pelanggaran (SIPP) Bakamla, terlebih dahulu periksa kelengkapan pengaduan Anda apakah telah sesuai dengan kriteria pengaduan yang telah ditetapkan yaitu mengandung unsur&nbsp;
            <strong>4W+1H</strong>&nbsp;(What, Who, Where, When dan How).&nbsp;
            <br>&nbsp;</li><li>Jika pengaduan tersebut telah memenuhi kriteria yang telah ditentukan, tahap berikutnya adalah mengisi formulir pengaduan dengan menekan tombol&nbsp;"BUAT PENGADUAN" yang terdapat pada menu navigasi Pengaduan di bagian atas halaman. 
              Silahkan mengisi semua data yang diminta secara lengkap dan benar dan lanjutkan dengan menekan tombol "Kirim Pengaduan".<br>&nbsp;</li>
            <li>Setelah mengirim pengaduan, secara otomatis anda telah membuat akun di aplikasi SIIP Bakamla.  
              (<strong>Jaga kerahasiaan username akun dan password Anda, agar tidak disalahgunakan oleh orang yang tidak bertanggung jawab</strong>).
              <br>&nbsp;</li><li> Jika pengaduan anda diterima selanjutnya akan dikonfirmasikan melalui email yang sudah anda cantumkan. Kami tidak meminta data pribadi yang berhubungan dengan Anda secara langsung kecuali jika tindak lanjut dari pengaduan tersebut membutuhkan data pribadi Anda.</li></ol>
        </div>
      </div>
    </div>

    {{-- footer --}}

    <footer class="text-white py-3" style="background-color: #07162b">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <p><b> BADAN KEAMANAN LAUT REPUBLIK INDONESIA</b></p>
            <p>Badan Keamanan Laut Republik Indonesia adalah badan yang bertugas melakukan patroli keamanan dan keselamatan
              di wilayah perairan Indonesia dan wilayah yurisdiksi Indonesia.</p>
            <hr>
            <p class="marker-address"><i class="fas fa-map-marker-alt fa-fw"></i> Bakamla Gedung Perintis Kemerdekaan</p>
            <p class="address">Jl. Proklamasi No.56, RT.10/RW.2, Pegangsaan, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10320</p>
          </div>
          <div class="col-lg-6 text-lg-end">
            <p>Hubungi Kami</p>
            <p><a href="https://wa.link/6fszq3" class="text-white">Whatsapp<i class="fab fa-whatsapp ms-3" style="font-size:6mm"></i></a>
            <p><a href="tel:+622150858130" class="text-white"> (021) 5084 8130<i class="fas fa-phone-alt fa-fw ms-3"></i></a></p>
            <p><a href="mailto:humas@bakamla.go.id" class="text-white">humas@bakamla.go.id<i class="fas fa-envelope fa-fw ms-3"></i></a></p>
            <p>&copy; 2023 Badan Keamanan Laut Indonesia. All rights reserved.</p>
          </div>
        </div>
      </div>
  </footer>      
    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>

