<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BAKAMLA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    {{-- <link rel="stylesheet" href="css/style.css"> --}}
    <style type="text/css">
      #map {
        height: 400px;
      }
      .card-header1 {    
        width: 100%; /* Ubah lebar sesuai kebutuhan */
        background-color: rgba(0, 0, 0, 0.7);
        height: 6cm;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
        padding: 20px;
      }
  </style>

  </head>
  <style>
    a.text-black {
        text-decoration: none;
    }
    a.text-white {
        text-decoration: none;
    }
    
</style>
  <body>
    {{-- Navbar --}}
    <div class="bg-light">
        <nav class="container navbar">
              <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                  <img src="{{asset('image/logo.png')}}" alt="Logo" width="100" height="60">
                </a>
                <div>
                  <p class="m-0 fs-5" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif"><b>BADAN KEAMANAN LAUT</b></p>
                  <p class="m-0 fs-6" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">REPUBLIK INDONESIA</p>
                </div>
              </div>
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link text-danger" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif" aria-current="page" href="{{route('dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger  dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Informasi</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('tentangwbs')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tentang SIPP</a></li>
                      <li><a class="dropdown-item" href="{{route ('dasarhukum')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Dasar Hukum</a></li>
                      <li><a class="dropdown-item" href="{{route ('dtpelanggaran')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pelanggaran</a></li>
                      <li><a class="dropdown-item" href="{{route ('alurpengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Alur Pengaduan</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li><a class="dropdown-item" href="{{route ('tatacara')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Tata Cara</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-danger dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Bantuan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="{{route ('faq')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">FAQ</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" style="color: #07162b;font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Pengaduan</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item nav-item active bg-danger text-white" href="{{route ('pengaduan')}}" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif">Buat Pengaduan</a></li>
                    </ul>
                </li>
              </ul>
        </nav>
    </div>

    {{-- konten --}}
    
    <div class="container-fluid p-0">
        <div class="text-white" style="background-image: url({{asset('image/abc.jpg')}})">
          <div class="card-header1 text-center py-5">
            <h1 class="mt-4" style="font-family: sans-serif"><b>Buat Pengaduan</b></h1>
          </div>
        </div>
    </div>

    <div class="container">
      <div class="card shadow p-3 mb-5 mt-5 bg-body rounded">
        <div class="card-header shadow p-3 mb-3 mt-3 rounded text-center" style="font-size: 20px;background-color: #07162b">
          <b style="color: yellow">FORM</b> <a class="text-white">PENGADUAN</a> 
        </div>
        <div class="card-body">
          <div class="alert alert-warning" role="alert">
            Silahkan anda membaca informasi berikut terlebih dahulu:
            <ul>
              <li>Pada bagian "Data Pribadi", diisi dengan data yang tidak melekat langsung pada diri Anda (pergunakan data samaran)</li>
              <li>Setelah Anda berhasil mengirimkan pengaduan, maka secara otomatis Anda memiliki akun di aplikasi WBS Bakamla dan akan mendapatkan ID pengaduan Anda</li>
              <li>Jaga kerahasiaan data akun Anda (Username &amp; Password), agar tidak disalahgunakan oleh orang-orang yang tidak bertanggung jawab</li>
            </ul>
          </div>
    
        
      
    
    <div class="card shadow-sm p-3 mb-5 mt-5 bg-body rounded">
      <div class="card-body ">
          <hr>
          @if (Auth::check())
          @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
                @endif
          <div class="card bg-danger text-white" >
            <div class="card-header " style="height: 4rem;font-size: 20px;background-color: #07162b">
              <label class="position-absolute top-50 start-0 translate-middle-y ms-2">Halo {{Auth::user()->name}} kamu login sebagai {{Auth::user()->role}}</label>
              <a class="btn btn-light position-absolute top-50 end-0 translate-middle-y mr-2" href="{{route('actionlogout')}}"><i class="fa fa-power-off"></i> Log Out</a>
            </div>
          </div>
          
            
        
              <form action="{{route('tbpengaduan')}}" method="post" class="p-3">
                @csrf
                <input type="hidden" name="email" value="{{ Auth::user()->id }}">
                <input type="hidden" name="status" value="diproses">
                  <div class="form-group">
                    <label><i class="fa fa-user"></i> Nama Pelapor</label>
                    <input type="text" name="nama_pelapor" class="form-control" placeholder="Isi dengan Nama Pelapor, dapat diisi lebih dari satu nama" required="">
                  </div>
                  <div class="form-group">
                    <label><i class="fa fa-id-card"></i> Unit Kerja Pelapor</label>
                      <input type="text" name="unit_kerja_pelapor" class="form-control" placeholder="Isi dengan unit kerja Pelapor" required="">
                    </div>
                    <div class="form-group">
                      <label><i class="fa fa-user-circle"></i> Jabatan Pelapor </label>
                      <input type="text" name="jabatan_pelapor" class="form-control" placeholder="Isi dengan jabatan Pelapor" required="">
                    </div>
                  <div class="form-group">
                      <label><i class="fa fa-calendar"></i> Tanggal Kejadian </label>
                      <input type="date" name="tanggal_kejadian" class="form-control " placeholder="Tanggal Kejadian" required="">
                  </div>
                  <div class="form-group">
                      <label><i class="fa fa-map-marker"></i> Lokasi Kejadian </label>
                      <div class="form-group" id="map"></div>
                      <input type="hidden" name="lokasi1" id="lokasi" required>
                  </div>
                  <div class="form-group">
                    <label><i class="fa fa-pencil"></i> Uraian Lengkap Kejadian </label>
                    <input type="text" name="uraian_lengkap_kejadian" class="form-control" placeholder="Uraian Lengkap Kejadian" required="">
                    <p style="font-size: 13px; color:gray">Isi dengan uraian lengkap / kronologi kejadian</p>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1" class="fa fa-edit">Dugaan Jenis Pelanggaran</label>
                    <!-- gunakan event onchange untuk mengirim data secara otomatis  -->
                    <select class="form-control" name="dugaan_jenis_pelanggaran">
                      @foreach ($jenis as $item)     
                      <option value="{{$item->id}}">{{$item->jenispelanggaran}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label><i class="fa fa-clock"></i> Waktu Perkiraan Kejadian </label>
                    <input type="text" name="waktu_perkiraan_kejadian" class="form-control" placeholder="Waktu Perkiraan Kejadian" required="">
                    <p style="font-size: 13px; color:gray">Contoh: Minggu Ketiga April 2021, Pukul 13:00 WIB, Pada saat tender pengadaan barang tahun 2021 dan lain-lain</p>
                  </div>
                  <button type="submit" class="btn" style="background-color: #07162b;color:white"><i class="fa fa-user"></i> SIMPAN</button>
                  <hr>
                  
                </form>
                @else
                
                  @if(session('message'))
                    <div class="alert alert-success">
                      {{session('message')}}
                    </div>
                  @endif
                  <p>Apakah Pernah Melakukan Pengaduan Sebelumnya?</p>
                  <form method="post" action="/login-register-ajax">
                    @csrf
                    <select class="form-select" aria-label="Default select example" name="option">
                      <option selected>---pilih---</option>
                      <option value="login">Sudah Pernah</option>
                      <option value="register">Belum Pernah</option>
                    </select>
                    <div class="mt-3 mb-3">
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                  </form>
                  
                  @if(isset($form))
                    <div id="form-container">
                      {!! $form !!}
                    </div>
                  @endif
                  
                  <script ></script>
                  
    
                  <script src="https://code.jquery.com/jquery-3.6.0.min.js" type="text/javascript">
                    $(document).ready(function() {
                      $('input[name="option"]').change(function() {
                        var option = $(this).val();
                  
                        $.post('/login-register-ajax', { option: option }, function(response) {
                          $('#form-container').html(response);
                        });
                      });
                    });
                  </script>
              @endif
          
          </div>
        </div>
      </div> 
    </div> 
    </div>

    {{-- footer --}}

    <footer class="text-white py-3" style="background-color: #07162b">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <p><b> BADAN KEAMANAN LAUT REPUBLIK INDONESIA</b></p>
            <p>Badan Keamanan Laut Republik Indonesia adalah badan yang bertugas melakukan patroli keamanan dan keselamatan
              di wilayah perairan Indonesia dan wilayah yurisdiksi Indonesia.</p>
            <hr>
            <p class="marker-address"><i class="fas fa-map-marker-alt fa-fw"></i> Bakamla Gedung Perintis Kemerdekaan</p>
            <p class="address">Jl. Proklamasi No.56, RT.10/RW.2, Pegangsaan, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10320</p>
          </div>
          <div class="col-lg-6 text-lg-end">
            <p>Hubungi Kami</p>
            <p><a href="https://wa.link/6fszq3" class="text-white">Whatsapp<i class="fab fa-whatsapp ms-3" style="font-size:6mm"></i></a>
            <p><a href="tel:+622150858130" class="text-white"> (021) 5084 8130<i class="fas fa-phone-alt fa-fw ms-3"></i></a></p>
            <p><a href="mailto:humas@bakamla.go.id" class="text-white">humas@bakamla.go.id<i class="fas fa-envelope fa-fw ms-3"></i></a></p>
            <p>&copy; 2023 Badan Keamanan Laut Indonesia. All rights reserved.</p>
          </div>
        </div>
      </div>
  </footer>  
  
  <script type="text/javascript">
    function initMap() {
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 5,
            center: { lat: 22.2734719, lng: 70.7512559 },
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };

                    map.setCenter(pos);
                    document.getElementById('lokasi').value = pos.lat + ',' + pos.lng;
                    placeMarker(pos, map);
                },
                () => {
                    // Handle jika gagal mendapatkan lokasi saat ini
                }
            );
        } else {
            // Handle jika browser tidak mendukung geolokasi
        }
    }

    function placeMarker(location, map) {
        const marker = new google.maps.Marker({
            position: location,
            map: map,
            title: "Lokasi Terpilih",
        });

        google.maps.event.addListener(map, 'click', function(event) {
            marker.setPosition(event.latLng);
            document.getElementById('lokasi').value = event.latLng.lat() + ',' + event.latLng.lng();
        });
    }
</script>



  <script type="text/javascript"
  src="https://maps.google.com/maps/api/js?key=AIzaSyBPKgg0js41JCvoFVlokdWUles15Po91RE&callback=initMap" ></script>

    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>


    