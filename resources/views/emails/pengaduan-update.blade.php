<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Status Pengaduan Telah Diperbarui</title>
</head>
<body>
    <h1>Notifikasi Pengaduan</h1>
    <p>Halo, {{ $data['nama_pelapor'] }}!</p>
    <p>Status pengaduan kamu dengan uraian lengkap kejadian sebagai berikut:</p>
    <p>{{ $data['uraian_lengkap_kejadian'] }}</p>
    <p>Telah diperbarui menjadi: {{ $data['status'] }}</p>
    <p>Terima kasih atas partisipasi dan perhatian kamu.</p>
</body>
</html>
