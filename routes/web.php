<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\kirimEmailController;
use App\Models\jenispelanggaran;
use App\Models\pengaduan;
use App\Models\tambahdt;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
      
Route::get('/', function () {
    return view('welcome');
});


Route::get('login', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('editjenispl/{id}', [HomeController::class, 'editjenispl'])->name('editjenispl')->middleware('auth');
Route::get('hapusjenispl/{id}', [HomeController::class, 'hapusjenispl'])->name('hapusjenispl')->middleware('auth');
Route::put('simpaneditpl/{id}', [HomeController::class, 'simpaneditpl'])->name('simpaneditpl')->middleware('auth');
Route::post('tambah1', [HomeController::class, 'tambah1'])->name('tambah1')->middleware('auth');
Route::get('jenispl/{id}', [HomeController::class, 'jenispl'])->name('jenispl')->middleware('auth');
Route::get('cetakdata', [HomeController::class, 'cetakdata'])->name('cetakdata')->middleware('auth');
Route::get('jenispl1/{id}', [HomeController::class, 'jenispl1'])->name('jenispl1')->middleware('auth');
Route::get('jenispl2/{id}', [HomeController::class, 'jenispl2'])->name('jenispl2');
Route::post('tbpengaduan', [HomeController::class, 'tbpengaduan'])->name('tbpengaduan');
Route::put('/pengaduan/{id}', [HomeController::class, 'update'])->name('pengaduan.update');
Route::get('hapuspengaduan/{id}', [HomeController::class, 'hapuspengaduan'])->name('hapuspengaduan')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');

Route::post('/login-register-ajax', [HomeController::class, 'loginRegisterAjax'])->name('login-register-ajax');

Route::post('register', [LoginController::class, 'register'])->name('register');

// Route::get('/', [kirimEmailController::class, 'index']);


Route::get('dashadmin', [HomeController::class, 'dashadmin'])->name('dashadmin')->middleware('auth');
Route::get('homeadmin', [HomeController::class, 'homeadmin'])->name('homeadmin')->middleware('auth');
Route::get('dtpengaduan', [HomeController::class, 'dtpengaduan'])->name('dtpengaduan')->middleware('auth');
Route::get('tambahdt', [HomeController::class, 'tambahdt'])->name('tambahdt')->middleware('auth');

Route::get('dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
Route::get('tentangwbs', [HomeController::class, 'tentangwbs'])->name('tentangwbs');
Route::get('dasarhukum', [HomeController::class, 'dasarhukum'])->name('dasarhukum');
Route::get('alurpengaduan', [HomeController::class, 'alurpengaduan'])->name('alurpengaduan');
Route::get('tatacara', [HomeController::class, 'tatacara'])->name('tatacara');
Route::get('faq', [HomeController::class, 'faq'])->name('faq');
Route::get('dtpelanggaran', [HomeController::class, 'dtpelanggaran'])->name('dtpelanggaran');
Route::get('pengaduan', [HomeController::class, 'pengaduan'])->name('pengaduan');
Route::get('homeuser', [HomeController::class, 'homeuser'])->name('homeuser');
Route::get('master1', [HomeController::class, 'master1'])->name('master1');

